# Zegetech Rails App
Getting started with the app

## Clone Repo
```
https://gitlab.com/gathuku/zegetech.git
```

Run with docker
```
docker-compose up
```

Alternatively you can run the image directly from the registry

```
docker pull registry.gitlab.com/gathuku/zegetech:latest
```

View a list of build images tags 

```
https://gitlab.com/gathuku/zegetech/container_registry
```

